# pxlBox SF4 Skeleton

pxlBox Symfony Skeleton is an extension for the official Symfony Skeleton (recommended way for starting new projects using Symfony Flex). It's main idea is to keep simplicity of official Skeleton, while adding must-have dependencies and default configs used in pxlBox for developing majority of the projects.